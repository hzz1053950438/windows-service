﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KQFuJiaCompareService
{
    /// <summary>
    /// 
    /// </summary>
    public class JsonObjectConvert
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="jsonText"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetObjectValue(string jsonText, string key)
        {
            //string jsonText = "{\"zone\":\"海淀\",\"zone_en\":\"haidian\"}";
            JObject jo = (JObject)JsonConvert.DeserializeObject(jsonText);
            if (jo == null || jo[key] == null)
            {
                return "";
            }
            string zone = jo[key].ToString();
            return zone;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="jsonData"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static JArray GetData2JArray(string jsonData, string key)
        {
            JObject obj = JObject.Parse(jsonData);
            return (JArray)obj[key];
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="jsonText"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static object GetObject(string jsonText, string key)
        {
            JObject jo = (JObject)JsonConvert.DeserializeObject(jsonText);
            var zone = jo[key];
            return zone;
        }
    }
}
