﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace TopShelf_WindowsServce.Common
{
   public class ConfigHelper
    {
        /// <summary>
        /// 根据Key取Value值
        /// </summary>
        /// <param name="key"></param>
        public static string AppSettings(string key)
        {
            return ConfigurationManager.AppSettings[key].ToString().Trim();
        }
        /// <summary>
        /// 根据name取connectionString值
        /// </summary>
        /// <param name="name"></param>
        public static string ConnectionStrings(string name)
        {
            return ConfigurationManager.ConnectionStrings[name].ConnectionString.Trim();
        }
        /// <summary>
        /// 获取主机Ip
        /// </summary>
        /// <returns></returns>
        public static string GetHostIP() {
            IPHostEntry ipEntry = Dns.GetHostEntry(Dns.GetHostName());
            return ipEntry.AddressList[1].ToString();
        }
        /// <summary>
        /// 获取服务名称
        /// </summary>
        /// <returns></returns>
        public static string GetServiceName()
        {
            
            return AppSettings("ServiceName");
        }
        /// <summary>
        /// 获取服务显示名称
        /// </summary>
        /// <returns></returns>
        public static string GetServiceShowName()
        {
            return AppSettings("ServiceShowName");
        }
        public static string GetServiceDescription()
        {
            return AppSettings("ServiceDescription");
        }
    }
}
