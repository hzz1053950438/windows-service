﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TopShelf_WindowsServce.Common;

namespace TopShelf_WindowsServce
{
    public class Base_Main
    {
        private string IP = string.Empty;

        public Base_Main() {
            IP = ConfigHelper.GetHostIP();
        }
        #region 开启服务
        public void StartService() {
            try
            {
                LogHelper.WriteLine(IP, "服务已启动");
                //开启多线程
                this.Multi_Task();
                LogHelper.WriteLine(IP, "服务执行完成");
            }
            catch (Exception ex)
            {
                LogHelper.WriteLine(IP, "Start" + ex.Message);
            }
        }
        #endregion
        #region 停止服务
        public void StopService()
        {
            try
            {
                LogHelper.WriteLine(IP, "服务停止");
            }
            catch (Exception ex)
            {
                LogHelper.WriteLine(IP, "Start" + ex.Message);
            }
        }

        #endregion

        /// <summary>
        /// 多线程
        /// </summary>
        public void Multi_Task() {
            List<Task> tasks = new List<Task>();

            //线程1
            var task1 = Task.Run(() =>
            {
                this.Task_Run1();
            });
            tasks.Add(task1);

            //线程1
            var task2 = Task.Run(() =>
            {
                this.Task_Run2();
            });
            tasks.Add(task2);
            Task.WhenAll(tasks);
            LogHelper.WriteLine(IP, "多线程准备完毕，已启动");
        }
        /// <summary>
        /// 任务一
        /// </summary>
        public void Task_Run1() {
            while (true)
            {
                LogHelper.WriteLine(IP, "任务1：" + "11111");
                Thread.Sleep(100);
            }
        }
        /// <summary>
        /// 任务二
        /// </summary>
        public void Task_Run2()
        {
            while (true) {
                LogHelper.WriteLine(IP, "任务2：" + "22222");
                Thread.Sleep(1000);
            }
                
        }
    }
}
