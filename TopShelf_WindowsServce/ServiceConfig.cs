﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;
using TopShelf_WindowsServce.Common;

namespace TopShelf_WindowsServce
{
    public class ServiceConfig
    {
        public static void Configure() {

            // 配置和运行宿主服务
            var rc=HostFactory.Run(x =>
            {
                // 指定服务类型。这里设置为 Base_Main
                x.Service<Base_Main>(service =>
                {
                    //通过 new Base_Main() 构建一个服务实例 
                    service.ConstructUsing(name => new Base_Main());

                    // 当服务启动后执行什么
                    service.WhenStarted(tc => tc.StartService());

                    // 当服务停止后执行什么
                    service.WhenStopped(tc => tc.StopService());

                    // 服务用本地系统账号来运行
                    x.RunAsLocalSystem();

                    // 服务描述信息
                    x.SetDescription(ConfigHelper.GetServiceDescription());
                    // 服务显示名称
                    x.SetDisplayName(ConfigHelper.GetServiceShowName());
                    // 服务名称
                    x.SetServiceName(ConfigHelper.GetServiceName());
                    //设置延迟启动
                    x.StartAutomaticallyDelayed();
                });
            });
            //控制台使用ctrl+C停止服务，会走这里
            //转化退出编码     
            var exitCode = (int)Convert.ChangeType(rc, rc.GetTypeCode());
            //设置退出编码
            Environment.ExitCode = exitCode;
        }
    }
}
